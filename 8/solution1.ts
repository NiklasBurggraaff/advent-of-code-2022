export const x = "";

const inputText = await Deno.readTextFile("./input.txt");

const heights = inputText
    .split("\n")
    .map((x) => x.split("").map((x) => parseInt(x)));

let visible = 0;

for (let i = 0; i < heights.length; i++) {
    for (let j = 0; j < heights[i].length; j++) {
        const element = heights[i][j];

        let visible_top = true;
        for (let k = i - 1; k >= 0; k--) {
            if (heights[k][j] >= element) {
                visible_top = false;
                break;
            }
        }

        if (visible_top) {
            visible++;
            continue;
        }

        let visible_bottom = true;
        for (let k = i + 1; k < heights.length; k++) {
            if (heights[k][j] >= element) {
                visible_bottom = false;
                break;
            }
        }

        if (visible_bottom) {
            visible++;
            continue;
        }

        let visible_left = true;
        for (let k = j - 1; k >= 0; k--) {
            if (heights[i][k] >= element) {
                visible_left = false;
                break;
            }
        }

        if (visible_left) {
            visible++;
            continue;
        }

        let visible_right = true;
        for (let k = j + 1; k < heights[i].length; k++) {
            if (heights[i][k] >= element) {
                visible_right = false;
                break;
            }
        }

        if (visible_right) {
            visible++;
            continue;
        }
    }
}

console.log(visible);
