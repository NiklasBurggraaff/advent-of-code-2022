export const x = "";

const inputText = await Deno.readTextFile("./input.txt");

const heights = inputText
    .split("\n")
    .map((x) => x.split("").map((x) => parseInt(x)));

let best_score = 0;

for (let i = 0; i < heights.length; i++) {
    for (let j = 0; j < heights[i].length; j++) {
        const element = heights[i][j];

        let visible_top = 0;
        for (let k = i - 1; k >= 0; k--) {
            if (heights[k][j] >= element) {
                break;
            }
            visible_top++;
        }

        let visible_bottom = 0;
        for (let k = i + 1; k < heights.length; k++) {
            visible_bottom++;
            if (heights[k][j] >= element) {
                break;
            }
        }

        let visible_left = 0;
        for (let k = j - 1; k >= 0; k--) {
            if (heights[i][k] >= element) {
                break;
            }
            visible_left++;
        }

        let visible_right = 0;
        for (let k = j + 1; k < heights[i].length; k++) {
            visible_right++;
            if (heights[i][k] >= element) {
                break;
            }
        }

        const score =
            visible_top * visible_bottom * visible_left * visible_right;

        if (score > best_score) {
            best_score = score;
        }
    }
}

console.log(best_score);
