def solve(input):
    ret = 0

    for line in input:
        pair = line.split(",")
        first = pair[0].split("-")
        second = pair[1].split("-")

        first = [int(x) for x in first]
        second = [int(x) for x in second]

        if first[0] <= second[0] <= first[1] and first[0] <= second[1] <= first[1]:
            ret += 1
        elif second[0] <= first[0] <= second[1] and second[0] <= first[1] <= second[1]:
            ret += 1

    return ret


def main():
    with open("input.txt") as f:
        data = f.read().splitlines()
    print(solve(data))


if __name__ == "__main__":
    main()
