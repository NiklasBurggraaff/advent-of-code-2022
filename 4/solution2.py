def solve(input):
    ret = 0

    for line in input:
        pair = line.split(",")
        first = pair[0].split("-")
        second = pair[1].split("-")

        first = [int(x) for x in first]
        second = [int(x) for x in second]

        first_range = set(range(first[0], first[1] + 1))
        second_range = set(range(second[0], second[1] + 1))

        if first_range.intersection(second_range):
            ret += 1

    return ret


def main():
    with open("input.txt") as f:
        data = f.read().splitlines()
    print(solve(data))


if __name__ == "__main__":
    main()
