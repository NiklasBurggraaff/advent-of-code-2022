const input = Deno.readTextFileSync("input.txt");
const lines = input.split("\n");

let cycle = 0;
let x = 1;
let sum = 0;

function doCycle() {
    cycle++;

    if ((cycle - 20) % 40 === 0) {
        console.log("cycle", cycle);
        console.log("x", x);
        console.log("x * cycle", x * cycle);
        console.log("");
        sum += x * cycle;
    }
}

for (const line of lines) {
    const instruction = line.split(" ")[0];

    if (instruction === "noop") {
        doCycle();
    } else {
        doCycle();
        doCycle();
        const value = parseInt(line.split(" ")[1]);
        x += value;
    }
}

console.log(sum);
