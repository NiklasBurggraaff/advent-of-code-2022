const input = Deno.readTextFileSync("input.txt");
const lines = input.split("\n");

let cycle = 0;
let x = 1;

let line = "";

function doCycle() {
    cycle++;

    const pixel = cycle % 40;

    if (pixel - x >= 0 && pixel - x <= 2) {
        line += "#";
    } else {
        line += ".";
    }

    if (cycle % 40 === 0) {
        console.log(line);
        line = "";
    }
}

for (const line of lines) {
    const instruction = line.split(" ")[0];

    if (instruction === "noop") {
        doCycle();
    } else {
        doCycle();
        doCycle();
        const value = parseInt(line.split(" ")[1]);
        x += value;
    }
}
