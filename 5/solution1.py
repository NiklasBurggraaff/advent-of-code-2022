def read_crate_row(row, c):
    res = [None for i in range(c)]
    for i in range(c):
        index = i * 4 + 1
        if index >= len(row):
            break

        if row[index] != " ":
            res[i] = row[index]
    return res


def read_move_row(row):
    row = row.split(" ")
    return int(row[1]), int(row[3]) - 1, int(row[5]) - 1


def solve(input):
    empty_index = input.index("")

    input1 = input[:empty_index]
    input2 = input[empty_index + 1:]

    crates = input1[-1].split(" ")
    crates = [int(crate) - 1 for crate in crates if crate != ""]

    crate_content = [[] for _ in range(len(crates))]

    for i in range(len(input1) - 2, -1, -1):
        crate_row = read_crate_row(input1[i], len(crates))
        for j in range(len(crates)):
            if crate_row[j] is not None:
                crate_content[crates[j]].append(crate_row[j])

    for i in range(len(input2)):
        count, from_, to = read_move_row(input2[i])

        for j in range(count):
            crate = crate_content[from_].pop()
            crate_content[to].append(crate)

    out = ""

    for i in range(len(crate_content)):
        out += crate_content[i][-1]

    return out


def main():
    with open("input.txt") as f:
        data = f.read().splitlines()
    print(solve(data))


if __name__ == "__main__":
    main()
