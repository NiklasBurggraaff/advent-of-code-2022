def solve(input):
    elf_calories = [0]

    for line in input:
        if line == "":
            elf_calories.append(0)
        else:
            elf_calories[-1] += int(line)

    max_elf_calories = max(elf_calories)
    return max_elf_calories


def main():
    with open("input.txt") as f:
        data = f.read().splitlines()
    print(solve(data))


if __name__ == "__main__":
    main()
