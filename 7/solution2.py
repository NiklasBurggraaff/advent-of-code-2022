class File:
    def __init__(self, name: str, parent: str, size: int):
        self.name = name
        self.parent = parent
        self.size = size

    def get_path(self):
        if self.parent is None:
            return self.name
        else:
            return self.parent.get_path() + "/" + self.name

    def __repr__(self):
        return self.name


class Folder:
    def __init__(self, name, parent, files, folders):
        self.name: str = name
        self.parent: Folder = parent
        self.files: list[File] = []
        self.folders: list[Folder] = []

    def get_path(self):
        if self.parent is None:
            return self.name
        else:
            return self.parent.get_path() + "/" + self.name

    def get_size(self):
        size = 0
        for file in self.files:
            size += file.size
        for folder in self.folders:
            size += folder.get_size()
        return size

    def get_all_folders(self):
        folders = []
        for folder in self.folders:
            folders.append(folder)
            folders.extend(folder.get_all_folders())
        return folders

    def get_child_folder(self, name):
        for folder in self.folders:
            if folder.name == name:
                return folder
        return None

    def get_child_file(self, name):
        for file in self.files:
            if file.name == name:
                return file
        return None

    def __repr__(self):
        return self.name


def parse_cd(folder, arguments):
    if arguments == "/":
        while folder.parent is not None:
            folder = folder.parent
        return folder

    if arguments == "..":
        return folder.parent

    return folder.get_child_folder(arguments)


def parse_ls(folder, input, i, command, arguments) -> int:
    while i < len(input):
        line = input[i]
        if line[0] == "$":
            break

        info, name = line.split(" ")
        if info == "dir":
            folder.folders.append(Folder(name, folder, [], []))
        else:
            folder.files.append(File(name, folder, int(info)))

        i += 1

    return i


def parse_input(root, input):
    folder = root
    i = 0
    while i < len(input):
        line = input[i].split(" ")
        if line[0] != "$":
            raise Exception("Expected command")

        command = line[1]
        arguments = line[2:]

        i += 1

        if command == "cd":
            folder = parse_cd(folder, arguments[0])
        elif command == "ls":
            i = parse_ls(folder, input, i, command, arguments)


def solve(input):
    assert input[0] == "$ cd /"
    input.pop(0)

    root = Folder("/", None, [], [])
    parse_input(root, input)

    size = root.get_size()
    storage_to_delete = size - (70000000 - 30000000)

    best_size = size
    for folder in root.get_all_folders():
        size = folder.get_size()
        if size >= storage_to_delete and size < best_size:
            best_size = size

    return best_size


def main():
    with open("input.txt") as f:
        data = f.read().splitlines()
    print(solve(data))


if __name__ == "__main__":
    main()
