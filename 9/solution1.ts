const input = Deno.readTextFileSync("input.txt");
const lines = input.split("\n");

type Position = {
    x: number;
    y: number;
};

const head_pos: Position = { x: 0, y: 0 };
const tail_pos: Position = { x: 0, y: 0 };

const tail_history: Set<string> = new Set();
tail_history.add("0,0");
console.log("0,0");

for (const line of lines) {
    const [direction, distance] = [line[0], parseInt(line.slice(1))];
    console.log(direction, distance);

    for (let i = 0; i < distance; i++) {
        switch (direction) {
            case "U":
                head_pos.y += 1;
                break;
            case "D":
                head_pos.y -= 1;
                break;
            case "L":
                head_pos.x -= 1;
                break;
            case "R":
                head_pos.x += 1;
                break;
            default:
                throw new Error("Invalid direction");
        }

        const dx = head_pos.x - tail_pos.x;
        const dy = head_pos.y - tail_pos.y;

        if (Math.abs(dx) <= 1 && Math.abs(dy) <= 1) {
            continue;
        }

        // Get sign of dx and dy
        const sx = dx === 0 ? 0 : dx / Math.abs(dx);
        const sy = dy === 0 ? 0 : dy / Math.abs(dy);

        tail_pos.x += sx;
        tail_pos.y += sy;

        tail_history.add(`${tail_pos.x},${tail_pos.y}`);
        console.log(`${tail_pos.x},${tail_pos.y}`);
    }

    console.log("-----");
}

console.log(tail_history);
console.log(tail_history.size);
