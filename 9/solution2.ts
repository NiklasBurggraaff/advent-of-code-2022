const input = Deno.readTextFileSync("input.txt");
const lines = input.split("\n");

type Position = {
    x: number;
    y: number;
};

// Create array of 10 positions at 0,0
const knots: Array<Position> = [
    { x: 0, y: 0 },
    { x: 0, y: 0 },
    { x: 0, y: 0 },
    { x: 0, y: 0 },
    { x: 0, y: 0 },
    { x: 0, y: 0 },
    { x: 0, y: 0 },
    { x: 0, y: 0 },
    { x: 0, y: 0 },
    { x: 0, y: 0 },
];

const tail_history: Set<string> = new Set();
tail_history.add("0,0");
console.log("0,0");

for (const line of lines) {
    const [direction, distance] = [line[0], parseInt(line.slice(1))];
    console.log(direction, distance);

    for (let i = 0; i < distance; i++) {
        switch (direction) {
            case "U":
                knots[0].y += 1;
                break;
            case "D":
                knots[0].y -= 1;
                break;
            case "L":
                knots[0].x -= 1;
                break;
            case "R":
                knots[0].x += 1;
                break;
            default:
                throw new Error("Invalid direction");
        }

        for (let j = 1; j < knots.length; j++) {
            const dx = knots[j - 1].x - knots[j].x;
            const dy = knots[j - 1].y - knots[j].y;

            if (Math.abs(dx) <= 1 && Math.abs(dy) <= 1) {
                break;
            }

            // Get sign of dx and dy
            const sx = dx === 0 ? 0 : dx / Math.abs(dx);
            const sy = dy === 0 ? 0 : dy / Math.abs(dy);

            knots[j].x += sx;
            knots[j].y += sy;
        }

        tail_history.add(`${knots[9].x},${knots[9].y}`);
        console.log(`${knots[9].x},${knots[9].y}`);
    }

    console.log("-----");
}

console.log(tail_history.size);
