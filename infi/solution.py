import matplotlib.pyplot as plt
import numpy as np


class KerstMan:
    position = [0, 0]
    direction = 0  # 0 is north, 1 is north-east, 2 is east, etc.
    position_history = []

    def loop(self, d):
        dx = 0
        dy = 0
        if self.direction == 0:
            dy = d
        elif self.direction == 1:
            dx = d
            dy = d
        elif self.direction == 2:
            dx = d
        elif self.direction == 3:
            dx = d
            dy = -d
        elif self.direction == 4:
            dy = -d
        elif self.direction == 5:
            dx = -d
            dy = -d
        elif self.direction == 6:
            dx = -d
        elif self.direction == 7:
            dx = -d
            dy = d

        self.position[0] += dx
        self.position[1] += dy

    def draai(self, x):
        self.direction += x / 45
        self.direction %= 8

    def spring(self, x):
        self.loop(x)

    def maak_stap(self, stap, x):
        if stap == "draai":
            self.draai(x)
        elif stap == "loop":
            initial_position = self.position[:]
            self.loop(x)
            self.position_history.append([initial_position, self.position[:]])
        elif stap == "spring":
            self.spring(x)
        else:
            raise ValueError(f"Unknown step {stap}")


def solve(input):
    kerstman = KerstMan()
    for line in input:
        stap, x = line.split()
        kerstman.maak_stap(stap, int(x))
        print(kerstman.position)

    ret = abs(kerstman.position[0]) + abs(kerstman.position[1])

    for p1, p2 in kerstman.position_history:
        plt.plot([p1[0], p2[0]], [p1[1], p2[1]], color="black")
    plt.show()

    return ret


def main():
    with open("input.txt") as f:
        data = f.read().splitlines()
    print(solve(data))


if __name__ == "__main__":
    main()
