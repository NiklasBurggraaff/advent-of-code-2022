def solve1(input):
    for i in range(len(input) - 4):
        marker = set(input[i:i+4])
        if len(marker) == 4:
            return i + 4


def solve(input):
    input = input[0]
    index = solve1(input)
    for i in range(index, len(input) - 14):
        marker = set(input[i:i+14])
        if len(marker) == 14:
            return i + 14


def main():
    with open("input.txt") as f:
        data = f.read().splitlines()
    print(solve(data))


if __name__ == "__main__":
    main()
