from enum import Enum


class Choice(Enum):
    ROCK = 1
    PAPER = 2
    SCISSORS = 3

    @classmethod
    def fromOpponent(cls, opponent) -> "Choice":
        if opponent == "A":
            return cls.ROCK
        elif opponent == "B":
            return cls.PAPER
        elif opponent == "C":
            return cls.SCISSORS

    @classmethod
    def fromMy(cls, opponent) -> "Choice":
        if opponent == "X":
            return cls.ROCK
        elif opponent == "Y":
            return cls.PAPER
        elif opponent == "Z":
            return cls.SCISSORS


def result(myChoice, opponentChoice):
    if myChoice == opponentChoice:
        return 0
    elif myChoice == Choice.ROCK:
        if opponentChoice == Choice.SCISSORS:
            return 1
        else:
            return -1
    elif myChoice == Choice.PAPER:
        if opponentChoice == Choice.ROCK:
            return 1
        else:
            return -1
    elif myChoice == Choice.SCISSORS:
        if opponentChoice == Choice.PAPER:
            return 1
        else:
            return -1


def solve(input):
    score = 0

    for line in input:
        opponent_choice = Choice.fromOpponent(line.split(" ")[0])
        my_choice = Choice.fromMy(line.split(" ")[1])

        res = result(my_choice, opponent_choice)

        score += my_choice.value
        if res == 1:
            score += 6
        elif res == 0:
            score += 3

    return score


def main():
    with open("input.txt") as f:
        data = f.read().splitlines()
    print(solve(data))


if __name__ == "__main__":
    main()
