def get_score(item):
    if item.islower():
        return ord(item) - ord('a') + 1
    else:
        return ord(item) - ord('A') + 27


def solve(input):
    sum = 0

    for i in range(len(input)//3):
        r1 = input[i*3]
        r2 = input[i*3+1]
        r3 = input[i*3+2]

        shared_items = set(r1).intersection(set(r2)).intersection(set(r3))
        shared_item = shared_items.pop()
        sum += get_score(shared_item)

    return sum


def main():
    with open("input.txt") as f:
        data = f.read().splitlines()
    print(solve(data))


if __name__ == "__main__":
    main()
