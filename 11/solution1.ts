type Monkey = {
    index: number;
    itemWorryLevels: number[];
    operation: (a: number) => number;
    test: (a: number) => boolean;
    ifTrue: number;
    ifFalse: number;
};

function printMonkey(monkey: Monkey) {
    console.log(`Monkey ${monkey.index}: ${monkey.itemWorryLevels}`);
}

function printMonkeys(monkeys: Monkey[]) {
    monkeys.forEach(printMonkey);
    console.log();
}

const monkeys: Monkey[] = [
    {
        index: 0,
        itemWorryLevels: [79, 98],
        operation: (old) => old * 19,
        test: (old) => old % 23 === 0,
        ifTrue: 2,
        ifFalse: 3,
    },
    {
        index: 1,
        itemWorryLevels: [54, 65, 75, 74],
        operation: (old) => old + 6,
        test: (old) => old % 19 === 0,
        ifTrue: 2,
        ifFalse: 0,
    },
    {
        index: 2,
        itemWorryLevels: [79, 60, 97],
        operation: (old) => old * old,
        test: (old) => old % 13 === 0,
        ifTrue: 1,
        ifFalse: 3,
    },
    {
        index: 3,
        itemWorryLevels: [74],
        operation: (old) => old + 3,
        test: (old) => old % 17 === 0,
        ifTrue: 0,
        ifFalse: 1,
    },
];

const inspectedItems: number[] = monkeys.map(() => 0);

console.log("Starting monkeys:");
printMonkeys(monkeys);

for (let i = 0; i < 20; i++) {
    for (let m = 0; m < monkeys.length; m++) {
        const monkey = monkeys[m];
        const newItems: number[] = [];
        for (const item of monkey.itemWorryLevels) {
            inspectedItems[m]++;
            const newItem = Math.floor(monkey.operation(item) / 3);
            if (monkey.test(newItem)) {
                if (monkey.ifTrue === m) {
                    newItems.push(newItem);
                } else {
                    monkeys[monkey.ifTrue].itemWorryLevels.push(newItem);
                }
            } else {
                if (monkey.ifFalse === m) {
                    newItems.push(newItem);
                } else {
                    monkeys[monkey.ifFalse].itemWorryLevels.push(newItem);
                }
            }
        }
        monkey.itemWorryLevels = newItems;
    }

    console.log(`After ${i + 1} rounds:`);
    printMonkeys(monkeys);
}

console.log(inspectedItems);
inspectedItems.sort((a, b) => b - a);

const monkeyBusiness = inspectedItems[0] * inspectedItems[1];

console.log(inspectedItems);
console.log(monkeyBusiness);
