type Monkey = {
    index: number;
    itemWorryLevels: number[];
    operation: (a: number) => number;
    test: (a: number) => boolean;
    ifTrue: number;
    ifFalse: number;
};

function printMonkey(monkey: Monkey) {
    console.log(`Monkey ${monkey.index}: ${monkey.itemWorryLevels}`);
}

function printMonkeys(monkeys: Monkey[]) {
    monkeys.forEach(printMonkey);
    console.log();
}

const monkeys: Monkey[] = [
    {
        index: 0,
        itemWorryLevels: [63, 57],
        operation: (a: number) => a * 11,
        test: (a: number) => a % 7 === 0,
        ifTrue: 6,
        ifFalse: 2,
    },
    {
        index: 1,
        itemWorryLevels: [82, 66, 87, 78, 77, 92, 83],
        operation: (a: number) => a + 1,
        test: (a: number) => a % 11 === 0,
        ifTrue: 5,
        ifFalse: 0,
    },
    {
        index: 2,
        itemWorryLevels: [97, 53, 53, 85, 58, 54],
        operation: (a: number) => a * 7,
        test: (a: number) => a % 13 === 0,
        ifTrue: 4,
        ifFalse: 3,
    },
    {
        index: 3,
        itemWorryLevels: [50],
        operation: (a: number) => a + 3,
        test: (a: number) => a % 3 === 0,
        ifTrue: 1,
        ifFalse: 7,
    },
    {
        index: 4,
        itemWorryLevels: [64, 69, 52, 65, 73],
        operation: (a: number) => a + 6,
        test: (a: number) => a % 17 === 0,
        ifTrue: 3,
        ifFalse: 7,
    },
    {
        index: 5,
        itemWorryLevels: [57, 91, 65],
        operation: (a: number) => a + 5,
        test: (a: number) => a % 2 === 0,
        ifTrue: 0,
        ifFalse: 6,
    },
    {
        index: 6,
        itemWorryLevels: [67, 91, 84, 78, 60, 69, 99, 83],
        operation: (a: number) => a * a,
        test: (a: number) => a % 5 === 0,
        ifTrue: 2,
        ifFalse: 4,
    },
    {
        index: 7,
        itemWorryLevels: [58, 78, 69, 65],
        operation: (a: number) => a + 7,
        test: (a: number) => a % 19 === 0,
        ifTrue: 5,
        ifFalse: 1,
    },
];

const divisor = 7 * 11 * 13 * 17 * 19 * 2 * 3 * 5;

const inspectedItems: number[] = monkeys.map(() => 0);

console.log("Starting monkeys:");
printMonkeys(monkeys);

for (let i = 0; i < 10_000; i++) {
    for (let m = 0; m < monkeys.length; m++) {
        const monkey = monkeys[m];
        const newItems: number[] = [];
        for (const item of monkey.itemWorryLevels) {
            inspectedItems[m]++;
            const newItem = monkey.operation(item) % divisor;
            if (monkey.test(newItem)) {
                if (monkey.ifTrue === m) {
                    newItems.push(newItem);
                } else {
                    monkeys[monkey.ifTrue].itemWorryLevels.push(newItem);
                }
            } else {
                if (monkey.ifFalse === m) {
                    newItems.push(newItem);
                } else {
                    monkeys[monkey.ifFalse].itemWorryLevels.push(newItem);
                }
            }
        }
        monkey.itemWorryLevels = newItems;
    }

    // console.log(`After ${i + 1} rounds:`);
    // console.log(inspectedItems);
    // printMonkeys(monkeys);
    // console.log();
}

console.log(inspectedItems);
inspectedItems.sort((a, b) => b - a);

const monkeyBusiness = inspectedItems[0] * inspectedItems[1];

console.log(inspectedItems);
console.log(monkeyBusiness);
